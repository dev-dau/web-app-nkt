from django.shortcuts import render, redirect,get_object_or_404,HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, RegistrationForm , CreateUserForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import RegisterForm,UpdateForm
from .models import information_NKT,Permission
from django.http import Http404
from django.contrib import messages

import json
from django.conf import settings
import urllib.request


# Create your views here.
...

def form_valid(self, form):
    # get the token submitted in the form
    recaptcha_response = self.request.POST.get('g-recaptcha-response')
    url = 'https://www.google.com/recaptcha/api/siteverify'
    payload = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }
    data = urllib.parse.urlencode(payload).encode()
    req = urllib.request.Request(url, data=data)

    # verify the token submitted with the form is valid
    response = urllib.request.urlopen(req)
    result = json.loads(response.read().decode())

    # result will be a dict containing 'contact' and 'action'.
    # it is important to verify both

    if (not result['contact']) or (not result['action'] == ''):  # make sure action matches the one from your template
        messages.error(self.request, 'Invalid reCAPTCHA. Please try again.')
        return super().form_invalid(form)


@login_required(login_url='signin')
def base_main(request):
    return render(request, 'information_nkt/base_main.html')

# ---------- LƯU THÔNG TIN NKT ----------
@login_required(login_url='signin')
def info_NKT(request):
    actual_info = information_NKT()
    # ----- Phân quyền user ---------
    qs_permission = get_object_or_404(Permission,user_rel=request.user.id)
    if qs_permission.can_add or qs_permission.can_view: # ----- Kiểm tra nếu user có quyền thì mới được truy cập vào và thực hiện
        if request.method == 'POST':
            registerForm = RegisterForm(request.POST, instance=actual_info)
            if registerForm.is_valid():
                registerForm.save()
                return redirect('/info_NKT/')
                # return render(request, 'information_nkt/index.html',{'form_nhap':registerForm})
            else:
                print(registerForm.errors)
        else:
            registerForm = RegisterForm()
    else:                                   # ----- Còn ngược lại thì thông báo lỗi 
        return render(request, 'information_nkt/404.html')

    return render(request, 'information_nkt/index.html',{'form_nhap':registerForm})


# ---------- DANH SÁCH NKT ----------
@login_required(login_url='signin')
def ds_NKT(request):
    qs_permission = get_object_or_404(Permission,user_rel=request.user.id)
    if qs_permission.can_view:
        if request.method == 'GET':
            info = information_NKT.objects.all()

            context = {
                'info': info,
                'error': '',
            }
        else:
            print("POST")
        return render(request,'information_nkt/ds_NKT.html', context)
    else:
        return render(request, 'information_nkt/404.html')

# ---------- FORM ĐĂNG NHẬP ----------
def signin(request):
    forms = LoginForm()
    if request.method == 'POST':
        forms = LoginForm(request.POST)
        if forms.is_valid():
            username = forms.cleaned_data['username']
            password = forms.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('base_main')
            else:
                context = {
                    'form': forms,
                    'error': 'Tài khoản hoặc mật khẩu không đúng    ! Mời kiểm tra lại.'
                }
                return render(request, 'account/signin.html', context)
    context = {
        'form': forms,
        'error': ''
    }
    return render(request, 'account/signin.html', context)

# ---------- FORM ĐĂNG KÝ ----------
def signup(request):
    forms = RegistrationForm()
    if request.method == 'POST':
        forms = RegistrationForm(request.POST)
        if forms.is_valid():
            firstname = forms.cleaned_data['firstname']
            lastname = forms.cleaned_data['lastname']
            email = forms.cleaned_data['email']
            username = forms.cleaned_data['username']
            password = forms.cleaned_data['password']
            confirm_password = forms.cleaned_data['confirm_password']

            if password == confirm_password:
                try:
                    if User.objects.filter(email=email).exists():
                        context = {
                            'form': forms,
                            'error1': 'EMAIL đã tồn tại !', 
                        }
                        return render(request, 'account/signup.html', context)
                    else:
                        # ----- Phân quyền tự động cho user đăng kí tài khoản ---------
                        Permission(user_rel=User.objects.create_user(username=username, password=password, email=email, first_name=firstname, last_name=lastname),
                        can_view=True, can_add=True, can_edit=True, can_delete=False, can_report=False, can_manage_user=False).save()
                        return redirect('signin')
                except:
                    context = {
                        'form': forms,
                        'error': 'USERNAME đã tồn tại !', 
                    }
                    return render(request, 'account/signup.html', context)
    context = {
        'form': forms
    }
    return render(request, 'account/signup.html', context)



# ---------- FORM LOGOUT ----------
def signout(request):
    logout(request)
    return redirect('signin')
    
@login_required(login_url='signin')
def createUser(request):
    qs_permission_user = get_object_or_404(Permission,user_rel=request.user.id)
    if qs_permission_user.can_view or qs_permission_user.can_edit or qs_permission_user.can_delete or qs_permission_user.can_add:
        if request.method == 'GET':
            info_user = RegistrationForm.objects.all()

            context_user = {
                'info_user': info_user,
                'error_user': '',
            }
        else:
            print("POST")
        return render(request,'information_nkt/admindemo.html', context_user)
    else:
        return render(request, 'information_nkt/404.html')
# @login_required(login_url='signin')
# def createUser(request):
#     forms_user = CreateUserForm()
#     qs_permission = get_object_or_404(Permission,user_rel=request.user.id)
#     if qs_permission.can_view:
#         if request.method == 'GET':
#             infor_user = CreateUserForm.objects.all()

#             context = {
#                 'info_user': infor_user,
#                 'error': '',
#             }
#         elif request.method == 'POST':
#             forms_user = CreateUserForm(request.POST)
#             if forms_user.is_valid():
#                 ten_nguoi_dung = forms_user.cleaned_data['ten_nguoi_dung']
#                 mat_khau = forms_user.cleaned_data['mat_khau']
#                 xac_nhan_mat_khau = forms_user.cleaned_data['xac_nhan_mat_khau']
#                 ho_ten = forms_user.cleaned_data['ho_ten']
#                 email = forms_user.cleaned_data['email']
#                 di_dong = forms_user.cleaned_data['di_dong']

#                 if mat_khau == xac_nhan_mat_khau:
#                     try:
#                         if User.objects.filter(email=email).exists():
#                             context = {
#                                 'forms_user': forms_user,
#                                 'error_email': 'Email đã tồn tại!', 
#                             }
#                             return render(request, 'information_nkt/admindemo.html', context)
#                         else:
#                             # ----- Phân quyền tự động cho user đăng kí tài khoản ---------
#                             Permission(user_rel=User.objects.create_user(ten_nguoi_dung=ten_nguoi_dung, mat_khau=mat_khau,  xac_nhan_mat_khau=xac_nhan_mat_khau, email=email, ho_ten=ho_ten, di_dong=di_dong),
#                             can_view=True, can_add=True, can_edit=True, can_delete=True, can_report=True, can_manage_user=True).save()
#                             return redirect('signin')
#                     except:
#                         context = {
#                             'forms_user': forms_user,
#                             'error_tennguoidung': 'Tên người dùng đã tồn tại!', 
#                         }
#                         return render(request, 'information_nkt/admindemo.html', context)
#         context = {
#             'forms_user': forms_user
#         }
#         return render(request, 'information_nkt/admindemo.html', context)
    

# ---------- BÁO CÁO BIỂU ĐỒ DANH SÁCH ----------
def report(request):
    qs_permission = get_object_or_404(Permission,user_rel=request.user.id)
    if qs_permission.can_view:
        if request.method == 'GET':
            report = information_NKT.objects.all()

            context = {
                'report': report,
                'error': '',
            }
        else:
            print("POST")
        return render(request,'information_nkt/report_ds.html', context)
    else:
        return render(request, 'information_nkt/404.html')


# ---------- BÁO CÁO BIỂU ĐỒ THEO CHART ----------
def report_bd(request):
    qs_permission = get_object_or_404(Permission,user_rel=request.user.id)
    if qs_permission.can_delete:
        return render(request,'information_nkt/report_bd.html')
    else:
        return render(request,'information_nkt/404.html')


# --------------XEM THÔNG TIN CHI TIẾT NKT ----------------------
def view_NKT(request):
    if request.method == 'GET':
        view = information_NKT.objects.all()

        context = {
            'view': view,
            'error': '',
        }
    else:
        print("POST")
    return render(request,'information_nkt/ds_NKT.html', context)


# ------------------- XÓA THÔNG TIN NKT -------------------------
def ds_NKT_delete(request):
    if request.method == "GET":
        id = request.GET['id']
        delete = information_NKT.objects.get(id=id).delete()
        context = {
            'delete': delete,
            'error': '',
        }
    else:
        print("POST")
    return render(request,'information_nkt/ds_NKT.html', context)



# ------------------- FORM SỬA THÔNG TIN NKT -------------------------
def repair_info_nkt(request,pk=None):
    if pk:
        actual_info = information_NKT.objects.get(pk=pk)
    else:
        actual_info = information_NKT()


    registerForm1 = RegisterForm( instance=actual_info)
    # ----- Phân quyền user ---------
    qs_permission = get_object_or_404(Permission,user_rel=request.user.id)
    if qs_permission.can_add or qs_permission.can_view: # ----- Kiểm tra nếu user có quyền thì mới được truy cập vào và thực hiện
        if request.method == 'POST':
            registerForm1 = RegisterForm(request.POST, instance=actual_info)
            if registerForm1.is_valid():
                registerForm1.save()
                # print(registerForm1.cleaned_data)
                
                
                return redirect('/ds_NKT/')
                # return render(request, 'information_nkt/ds_NKT.html',{'form_sua':registerForm1,'pk':pk})
            else:
                print(registerForm1)

    else:                                
        return render(request, 'information_nkt/404.html')

    return render(request, 'information_nkt/repair_info_nkt.html',{'form_sua':registerForm1,'pk':pk})






