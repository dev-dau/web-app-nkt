from django.db import models
from django.contrib.auth.models import User
# from django.utils import timezone


# Create your models here.

#-----PHÂN QUYỀN USER SỬ DỤNG -----------
class Permission(models.Model):
    user_rel = models.ForeignKey(User,on_delete=models.CASCADE)
    can_view = models.BooleanField(default=True)
    can_add = models.BooleanField(default=True)
    can_edit = models.BooleanField(default=True)
    can_delete = models.BooleanField(default=True)
    can_report = models.BooleanField(default=True)
    can_manage_user = models.BooleanField(default=True)

    def __str__(self):
        return self.user_rel.username


#----- THÔNG TIN NGƯỜI KHUYẾT TẬT ------
class information_NKT(models.Model):
    ho_ten_khai_sinh_nkt = models.CharField(max_length=50,null=True)
    noi_dk_khai_sinh = models.CharField(max_length=100, null=True)
    que_quan = models.CharField(max_length=100, null=True)
    dia_chi_thuong_tru = models.CharField(max_length=100, null=True)
    so_tt_nkt = models.CharField(max_length=15,null=True,unique=True,error_messages={'unique':'Số thứ tự đã tồn tại'})
    ngay_sinh_nkt = models.DateField(null=True,blank=True)
    so_dinh_danh = models.CharField(max_length=20,null=True,unique=True,error_messages={'unique':'Số định danh đã tồn tại'})
    so_hieu_giay_xac_nhan = models.CharField(max_length=12,unique=True,error_messages={'unique':'Số hiệu đã tồn tại'})
    list_gioi_tinh = [
        ('Nam','Nam'),
        ('Nữ','Nữ'),
        ('Khác','Khác'),
    ]
    gioi_tinh = models.CharField(choices=list_gioi_tinh, max_length=20)

    so_cmnd = models.CharField(max_length=10, null=True, blank=True,unique=True,error_messages={'unique':'Số CMND đã tồn tại'})
    quoc_tich = models.CharField(max_length=20, null=True)
    list_dan_toc= [
        ('Kinh', 'Kinh'),
        ('Tày', 'Tày'),
        ('Thái', 'Thái'),
        ('Hoa', 'Hoa'),
    ]
    dan_toc= models.CharField( choices=list_dan_toc, max_length=200)

    list_ton_giao = [
        ('Thiên chúa', 'Thiên Chúa '),
        ('Phật giáo', 'Phật Giáo'),
        ('Không tôn giáo', 'Không tôn giáo'),
        ('Khác', 'Khác'),
    ] 
    ton_giao = models.CharField( choices=list_ton_giao, max_length=100)

    list_nhom_mau = [
        ('Nhóm máu O', 'Nhóm máu O'),
        ('Nhóm máu A', 'Nhóm máu A'),
        ('Nhóm máu B', 'Nhóm máu B'),
        ('Nhóm máu AB', 'Nhóm máu AB'),
    ]
    nhom_mau= models.CharField( choices=list_nhom_mau, max_length=200)

    list_trinh_do = [
        ('Không đi học/ Mù chữ', 'Không đi học/ Mù chữ'),
        ('Biết đọc, biết viết', 'Biết đọc, biết viết'),
        ('Tiểu học', 'Tiểu học'),
        ('THCS', 'THCS'),
        ('THPT', 'THPT'),
        ('Sơ cấp/ Trung học', 'Sơ cấp/ Trung học'),
        ('Cao đẳng/ Đại học', 'Cao đẳng/ Đại học'),
        ('Trên đại học', 'Trên đại học'),
        ('Khác', 'Khác'),
    ]
    trinh_do = models.CharField( choices=list_trinh_do, max_length=200)

    list_chuyen_mon =[
        ('Chưa qua đào tạo', 'Chưa qua đào tạo'),
        ('Trung học', 'Trung học'),
        ('Cao đẳng/ Đại học', 'Cao đẳng/ Đại học'),
        ('Trên đại học', 'Trên đại học'),
        ('Khác', 'Khác'),
    ]
    chuyen_mon = models.CharField(choices=list_chuyen_mon, max_length=100)

    list_nghe_nghiep =[
        ('Còn nhỏ', 'Còn nhỏ'),
        ('Nội trợ', 'Nội trợ'),
        ('Nông nghiệp', 'Nông nghiệp'),
        ('Trên đại học', 'Trên đại học'),
        ('Khác', 'Khác'),
    ]
    nghe_nghiep = models.CharField(choices=list_nghe_nghiep, max_length=100)

    list_nan_nhan_da_cam = [
        ('Có','Có'),
        ('Không','Không'),
        ('Khác', 'Khác'),
    ]
    nan_nhan_da_cam = models.CharField(choices=list_nan_nhan_da_cam, max_length=100)

    list_hon_nhan =[
        ('Chưa kết hôn', 'Chưa kết hôn'),
        ('Đã kết hôn', 'Đã kết hôn'),
        ('Ly hôn', 'Ly hôn'),
        ('Góa', 'Góa'),
        ('Khác', 'Khác'),
    ]
    hon_nhan = models.CharField(choices=list_hon_nhan, max_length=100)

    tong_so_con = models.CharField(max_length=2, null=True)
    so_con_duoi_16 = models.CharField(max_length=2, null=True)

    #----- THÔNG TIN LIÊN LẠC NKT -----
    dia_chi = models.CharField(max_length=100,null=True,blank=True)
    so_dt_lh = models.CharField(max_length=11,null=True,unique=True,error_messages={'unique':'Số điện thoại đã tồn tại'})
    ngay_cap_nhat = models.DateField(null=True)
    nguoi_cap_nhat = models.CharField(max_length=100,null=True)
    ho_ten_nguoi_ql = models.CharField(max_length=50,null=True)
    don_vi_cong_tac = models.CharField(max_length=50,null=True)
    linh_vuc = models.CharField(max_length=100,null=True)

    #----- THÔNG TIN HỘ GIA ĐÌNH -----
    ho_ten_chu_ho = models.CharField(max_length=30, null=True)
    so_cmnd_chu_ho = models.CharField(max_length=12, null=True)
    ma_so_dinh_danh_chu_ho = models.CharField(max_length=15, null=True)
    quoc_tich_chu_ho = models.CharField(max_length=15, null=True)

    list_quan_he_nkt = [
        ('Bố', 'Bố'),
        ('Mẹ', 'Mẹ'),
        ('Ông', 'Ông'),
        ('Bà', 'Bà'),
        ('Cô', 'Cô'),
        ('Dì', 'Dì'),
        ('Chú', 'Chú'),
        ('Bác', 'Bác'),
        ('Anh', 'Anh'),
        ('Chị', 'Chị'),
        ('Em', 'Em'),
        ('Chồng', 'Chồng'),
        ('Vợ', 'Vợ'),
        ('Con', 'Con'),
        ('Cháu', 'Cháu'),
        ('Người khuyết tật', 'Người khuyết tật'),
        ('Khác', 'Khác'),
    ]
    quan_he_voi_nkt = models.CharField(max_length=20,choices=list_quan_he_nkt)

    sdt_lien_lac_ho_gia_dinh = models.CharField(max_length=11, null=True)

    #----- THÔNG TIN NGƯỜI THÂN ĐẠI DIỆN -----
    ho_ten_nguoi_dai_dien = models.CharField(max_length=30, null=True)
    sdt_nguoi_dai_dien = models.CharField(max_length=11, null=True)
    so_cmnd_nguoi_dai_dien = models.CharField(max_length=30, null=True)
    ma_so_dinh_danh_nguoi_dai_dien = models.CharField(max_length=30, null=True)
    quoc_tich_nguoi_dai_dien = models.CharField(max_length=20, null=True)
    list_ndd_quan_he_nkt = [
        ('Bố', 'Bố'),
        ('Mẹ', 'Mẹ'),
        ('Ông', 'Ông'),
        ('Bà', 'Bà'),
        ('Cô', 'Cô'),
        ('Dì', 'Dì'),
        ('Chú', 'Chú'),
        ('Bác', 'Bác'),
        ('Anh', 'Anh'),
        ('Chị', 'Chị'),
        ('Em', 'Em'),
        ('Chồng', 'Chồng'),
        ('Vợ', 'Vợ'),
        ('Con', 'Con'),
        ('Cháu', 'Cháu'),
        ('Người khuyết tật', 'Người khuyết tật'),
        ('Khác', 'Khác'),
    ]
    ndd_quan_he_voi_nkt = models.CharField(max_length=20,choices=list_ndd_quan_he_nkt)

    #----- THÔNG TIN NGƯỜI CHĂM SÓC -----
    ho_ten_nguoi_cham_soc = models.CharField(max_length=20, null=True)
    so_cmnd_nguoi_cham_soc = models.CharField(max_length=15, null=True)
    ma_so_dinh_danh_nguoi_cham_soc = models.CharField(max_length=20, null=True)
    quoc_tich_nguoi_cham_soc = models.CharField(max_length=20, null=True)
    so_dt_nguoi_cham_soc = models.CharField(max_length=11, null=True)
    list_ncs_quan_he_nkt = [
        ('Bố', 'Bố'),
        ('Mẹ', 'Mẹ'),
        ('Ông', 'Ông'),
        ('Bà', 'Bà'),
        ('Cô', 'Cô'),
        ('Dì', 'Dì'),
        ('Chú', 'Chú'),
        ('Bác', 'Bác'),
        ('Anh', 'Anh'),
        ('Chị', 'Chị'),
        ('Em', 'Em'),
        ('Chồng', 'Chồng'),
        ('Vợ', 'Vợ'),
        ('Con', 'Con'),
        ('Cháu', 'Cháu'),
        ('Người khuyết tật', 'Người khuyết tật'),
        ('Khác', 'Khác'),
    ]
    ncs_quan_he_voi_nkt = models.CharField(max_length=20,choices=list_ncs_quan_he_nkt)

   

