from django import forms
from django.forms import ModelForm,ValidationError
from .models import information_NKT


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'name': 'username', 'placeholder': ' Tên đăng nhập'}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'password', 'name': 'password', 'placeholder': ' Mật khẩu'}))

class RegistrationForm(forms.Form):
    firstname = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'name': 'firstname', 'placeholder': 'Họ'}))
    lastname = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'name': 'lastname', 'placeholder': ' Tên'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'email', 'name': 'email', 'placeholder': ' Email'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'name': 'username', 'placeholder': 'Tên đăng nhâp'}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'password', 'name': 'password', 'placeholder': 'Mật khẩu'}))
    confirm_password = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'password', 'name': 'confirm-password', 'placeholder': 'Xác nhận mật khẩu'}))
    

class RegisterForm(forms.ModelForm):
    class Meta:
        model = information_NKT
        fields = '__all__' 

    ngay_sinh_nkt = forms.DateField(
        widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}),
        input_formats=('%m/%d/%Y', )
    )
    ngay_cap_nhat = forms.DateField(
        widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}),
        input_formats=('%m/%d/%Y',)
    )

    so_dinh_danh = forms.CharField(widget=forms.TextInput(attrs={ 'type': 'text',  'placeholder': ''}))

class UpdateForm(forms.ModelForm):
    class Meta:
        model = information_NKT
        fields = '__all__'

# tao user
class CreateUserForm(forms.Form):
    ten_nguoi_dung = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'placeholder': 'Tên người dùng'}))
    mat_khau = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'placeholder': 'Mật khẩu'}))
    xac_nhan_mat_khau = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'placeholder': 'Xác nhận mật khẩu'}))
    ho_ten = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'placeholder': 'Họ tên'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'placeholder': ' Email'}))
    di_dong = forms.CharField(widget=forms.TextInput(attrs={'class': 'input100', 'type': 'text', 'placeholder': 'Di động'}))
    ql_nguoidung = forms.BooleanField(required=False)
    ql_nhaplieu = forms.BooleanField(required=False)
    khach = forms.BooleanField(required=False)

